package trendnxt.HibernateAssignment_6_a;

import java.util.Iterator;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class ManageEmployeeData {
	
	public static void main(String[] args) {
		
		SessionFactory factory;
	    factory = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();
	      
		Session session=factory.openSession();
		Transaction tx=null;
		tx=session.beginTransaction();
		
	      Employee e1=new Employee();
	      e1.setEmployeeName("Pankaj Vijay");
	      e1.setEmployeeSalary(29787);
	      session.save(e1);
	      
	      Employee e2=new Employee();
	      e2.setEmployeeName("Shubham Kumar");
	      e2.setEmployeeSalary(31000);
	      session.save(e2);
	      
	      RegularEmployee rg=new RegularEmployee();
	      rg.setEmployeeName("Deepak Sirwani");
	      rg.setEmployeeSalary(34000);
	      rg.setQplc(4088);
	      session.save(rg);
	      
	      ContractEmployee ct=new ContractEmployee();
	      ct.setEmployeeName("Ayushi Singh");
	      ct.setEmployeeSalary(23000);
	      ct.setAllowance(7000);
	      session.save(ct);
	     tx.commit();
	     System.out.println("records added successfully to the database");
	      
	     /* Display the list of employees in the organization */
	     new ManageEmployeeData().listEmployees();
	     
	     
	     
	      /* Delete a student from the database */
	      new ManageEmployeeData().deleteEmployee(rg.getEmployeeId());

	      
	      /* Display the list of employees in the organization after deleting an employee */
		     new ManageEmployeeData().listEmployees();

	   }
		
	   /* Method to delete an employee from the records */
	   public void deleteEmployee(Integer EmployeeID){
		   
		   SessionFactory factory;
		   factory=new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();
	      Session session = factory.openSession();
	      Transaction tx = null;
	      
	      try {
	         tx = session.beginTransaction();
	         RegularEmployee rg = (RegularEmployee)session.get(RegularEmployee.class, EmployeeID); 
	         session.delete(rg); 
	         tx.commit();
	      } catch (HibernateException e) {
	         e.printStackTrace(); 
	      } finally {
	         session.close(); 
	      }
	      System.out.println("record deleted successfully");
	   }
	   
	   
	   public void listEmployees( ){
		   SessionFactory factory;
		   factory=new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();
		   Session session = factory.openSession();
	      Transaction tx = null;
	      
	      try {
	         tx = session.beginTransaction();
	         List employees = session.createQuery("FROM Employee").list(); 
	         for (Iterator iterator1 = employees.iterator(); iterator1.hasNext();){
	        	 Employee employee = (Employee) iterator1.next(); 
	            System.out.println("Empoyee Name: " + employee.getEmployeeName()); 
	            System.out.println("  Salary: " + employee.getEmployeeSalary()); 
	         }
	         tx.commit();
	      } catch (HibernateException e) {
	         if (tx!=null) tx.rollback();
	         e.printStackTrace(); 
	      } finally {
	         session.close(); 
	      }
	   }
}
