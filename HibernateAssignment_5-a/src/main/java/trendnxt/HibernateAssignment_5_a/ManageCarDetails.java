package trendnxt.HibernateAssignment_5_a;

import java.util.Iterator;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class ManageCarDetails {

	private static SessionFactory sessionFactory;
	
	public static void main(String[] args) {

		sessionFactory=new Configuration().configure().buildSessionFactory();
		
		Session session=sessionFactory.openSession();
		
		Transaction tx=session.beginTransaction();
		
		//assuming the table car has been created in the database as per the question
		Query query=session.createQuery("From Car");
		List list=query.list();
		
		Iterator itr=list.iterator();
		while(itr.hasNext()){
			
			Car car=(Car)itr.next();
			System.out.println("Car Details:\n"+"Car Reg No:"+car.getRegNo()+"\nCar Model:"+car.getModel()+"\nCar Color:"+car.getColor()+"Car Manufacturer:"+
			car.getManufacturer()+"\nCar Price:"+car.getPrice());
			
		}
		tx.commit();
	}

}
