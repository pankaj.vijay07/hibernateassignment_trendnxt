package trendnxt.HibernateAssign2;

import java.util.Iterator;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class ManageEmployeeData {

	private static SessionFactory factory;

	public static void main(String[] args) {

		factory = new Configuration().configure().buildSessionFactory();

		ManageEmployeeData data = new ManageEmployeeData();

		/* Add few employee records in database */
		Integer empID1 = data.addEmployee(1, "Pankaj Vijay", "TRB");
		Integer empID2 = data.addEmployee(2, "Ashok Saxena", "A3");
		Integer empID3 = data.addEmployee(3, "Paul Thomas", "A1");

		/* List down all the employees */
		data.listEmployees();

		/* Update employee's records */
		data.updateEmployee(empID1, "B3");

		/* Delete an employee from the database */
		data.deleteEmployee(empID2);

		/*
		 * List down new list of the employees after performing deletion and
		 * updation
		 */
		data.listEmployees();
	}

	/* Method to CREATE an employee in the database */
	public Integer addEmployee(int id, String name, String band) {
		Session session = factory.openSession();
		Transaction tx = null;
		Integer employeeID = null;

		tx = session.beginTransaction();
		Employee employee = new Employee(id, name, band);
		employeeID = (Integer) session.save(employee);
		tx.commit();
		return employeeID;
	}

	/* Method to READ all the employees */
	public void listEmployees() {
		Session session = factory.openSession();
		Transaction tx = null;

		try {
			tx = session.beginTransaction();

			List employees = session.createQuery("FROM Employee").list();
			for (Iterator iterator = employees.iterator(); iterator
					.hasNext();) {
				Employee employee = (Employee) iterator.next();
				System.out.print("Employee Id: " + employee.getEmpId());
				System.out.print("\nEmployee Name: " + employee.getEmpName());
				System.out.println("\nEmployee Band: " + employee.getEmpBand());
			}
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

	/* Method to UPDATE band for an employee */
	public void updateEmployee(Integer EmployeeID, String newBand) {
		Session session = factory.openSession();
		Transaction tx = null;

		try {
			tx = session.beginTransaction();
			Employee employee = (Employee) session.get(Employee.class,
					EmployeeID);
			employee.setEmpBand(newBand);
			session.update(employee);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

	/* Method to DELETE an employee from the records */
	public void deleteEmployee(Integer EmployeeID) {
		Session session = factory.openSession();
		Transaction tx = null;

		try {
			tx = session.beginTransaction();
			Employee employee = (Employee) session.get(Employee.class,
					EmployeeID);
			session.delete(employee);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

}
