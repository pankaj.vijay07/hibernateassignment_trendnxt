package trendnxt.HibernateAssign2;

public class Employee {

		private int empId;  
		private String empName,empBand;
		
		public Employee() {
			super();
		}

		public Employee(int empId, String empName, String empBand) {
			super();
			this.empId = empId;
			this.empName = empName;
			this.empBand = empBand;
		}
		
		public int getEmpId() {
			return empId;
		}
		public void setEmpId(int empId) {
			this.empId = empId;
		}
		public String getEmpName() {
			return empName;
		}
		public void setEmpName(String empName) {
			this.empName = empName;
		}
		public String getEmpBand() {
			return empBand;
		}
		public void setEmpBand(String empBand) {
			this.empBand = empBand;
		}
		
}
