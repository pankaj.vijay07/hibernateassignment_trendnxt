package trendnxt.HibernateAssignment_4_a;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class Client {

	// method to perform delete operation
		public void deleteDepartment(int deptID) {

			Configuration c = new Configuration();
			c.configure("hibernate.cfg.xml");

			SessionFactory sf = c.buildSessionFactory();

			Session session = sf.openSession();
			Transaction tx = null;
			try {
				tx = session.beginTransaction();
				Department department = (Department) session.get(Department.class,deptID);
				session.delete(department);
				tx.commit();
			} catch (HibernateException e) {
				e.printStackTrace();
			} finally {
				session.close();
			}
			
			System.out.println("Record deleted successfully");
		}

		/* Method to READ all the employees along with their departments */
		public void listEmployees() {

			Configuration c = new Configuration();
			c.configure("hibernate.cfg.xml");

			SessionFactory sf = c.buildSessionFactory();
			Session session = sf.openSession();
			Transaction tx = null;

			try {
				tx = session.beginTransaction();

				List employees = session.createQuery("FROM Department").list(); 
				Iterator iterator1 = employees.iterator();
				while(iterator1.hasNext()){
					Department department = (Department) iterator1.next(); 
		            System.out.println("Department name: " + department.getDeptName()); 
		            Set setOfEmployees = department.getEmployees();
		            for (Iterator iterator2 = setOfEmployees.iterator(); iterator2.hasNext();){
		               Employee emp = (Employee) iterator2.next(); 
		               System.out.println("Employee Name: " + emp.getEmployeeName());
		               System.out.println("Employee Salary: " + emp.getEmployeeSalary());
		            }
				}
				tx.commit();
			} catch (HibernateException e) {
				e.printStackTrace();
			} finally {
				session.close();
			}
		}

		public static void main(String[] args) {

			Set<Employee> setOfEmployees1 = new HashSet<Employee>();

			Set<Employee> setOfEmployees2 = new HashSet<Employee>();

			// TODO Auto-generated method stub

			Configuration c = new Configuration();
			c.configure("hibernate.cfg.xml");

			SessionFactory sf = c.buildSessionFactory();

			Session session = sf.openSession();
			Transaction trans = session.getTransaction();
			trans.begin();
			// create some Employee object
			Employee emp1 = new Employee();
			emp1.setEmployeeId(1);
			emp1.setEmployeeName("Ashok Saxena");
			emp1.setEmployeeSalary(30989);

			Employee emp2 = new Employee();
			emp2.setEmployeeId(2);
			emp2.setEmployeeName("Pooja Kumari");
			emp2.setEmployeeSalary(23900);

			Employee emp3 = new Employee();
			emp3.setEmployeeId(3);
			emp3.setEmployeeName("Pankaj Vijay");
			emp3.setEmployeeSalary(35000);

			Employee emp4 = new Employee();
			emp4.setEmployeeId(4);
			emp4.setEmployeeName("Prerna Sharma");
			emp4.setEmployeeSalary(37000);

			setOfEmployees1.add(emp1); // add Employee object to Set
			setOfEmployees1.add(emp2);
			setOfEmployees2.add(emp3);
			setOfEmployees2.add(emp4);

			Department dept1 = new Department();
			dept1.setDeptID(1234);
			dept1.setDeptName("Production");
			dept1.setEmployees(setOfEmployees1); // add Set object to Dept object
			session.persist(dept1);
			Department dept2 = new Department();
			dept2.setDeptID(1235);
			dept2.setDeptName("Engineering");
			dept2.setEmployees(setOfEmployees2); // add Set object to Dept object
			session.persist(dept2);
			trans.commit();
			System.out.println("Records inserted");

			/* Delete a department and its associated employees from the database */
			new Client().deleteDepartment(dept1.getDeptID());
			/*
			 * List down new list of the employees after performing deletion and
			 * updation
			 */
			new Client().listEmployees();

		}

}
