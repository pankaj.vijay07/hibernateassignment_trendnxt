package trendnxt.HibernateAssignment_4_a;

import java.util.Set;

public class Department {

	private int deptID;
	private String deptName;
	private Set employees;
	
	public Department() {
		super();
	}

	public int getDeptID() {
		return deptID;
	}

	public void setDeptID(int deptID) {
		this.deptID = deptID;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public Set getEmployees() {
		return employees;
	}

	public void setEmployees(Set employees) {
		this.employees = employees;
	}
}	