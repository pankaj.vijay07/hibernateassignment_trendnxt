package trendnxt.HibernateAssignment_3_a;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class ManageQuestions {

	
	private static SessionFactory factory;
	
	public static void main(String[] args) {

factory=new Configuration().configure().buildSessionFactory();

Session session=factory.openSession();
Transaction tx=session.beginTransaction();

ArrayList<String> list1=new ArrayList<String>();
list1.add("User friendly language");
list1.add("Platform dependent");

ArrayList<String> list2=new ArrayList<String>();
list2.add("An ancroym for Structured Query Language");
list2.add("Used to store data of an organization. Open Source");

Question question1=new Question();
question1.setQname("What is Python?");
question1.setAnswers(list1);

Question question2=new Question();
question2.setQname("What is Mysql?");
question2.setAnswers(list2);
	
session.persist(question1);    
session.persist(question2);    
    
tx.commit();    
session.close();    
System.out.println("success"); 	
	 
	
	
	
	
	
	
	
	}
}
