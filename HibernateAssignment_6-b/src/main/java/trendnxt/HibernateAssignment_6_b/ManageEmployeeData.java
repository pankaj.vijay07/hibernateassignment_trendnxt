package trendnxt.HibernateAssignment_6_b;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class ManageEmployeeData {
	
	public static void main(String[] args) {
		
		SessionFactory factory;
	    factory = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();
	      
		Session session=factory.openSession();
		Transaction tx=null;
		tx=session.beginTransaction();
		
	   RegularEmployee r1 = new RegularEmployee();
	    r1.setEmployeeId((short)100);
	    r1.setEmployeeName("S N Rao");
	    r1.setEmployeeSalary(70000);
	    r1.setQplc(10000);
		        
	    RegularEmployee r2 = new RegularEmployee();
	    r2.setEmployeeId((short)101);
	    r2.setEmployeeName("Sridhar");
	    r2.setEmployeeSalary(40000);
	    r2.setQplc(7000);
	 
	    ContractEmployee c1 = new ContractEmployee();
	    c1.setEmployeeId((short)102);
	    c1.setEmployeeName("Richa Grover");
	    c1.setEmployeeSalary(55000);
	    c1.setAllowance(15000);
			        
	    ContractEmployee c2 = new ContractEmployee();
	    c2.setEmployeeId((short)103);
	    c2.setEmployeeName("Aaushi Sharma");
	    c2.setEmployeeSalary(75000);
	    c2.setAllowance(25000);
	     		        
	    session.save(r1);  session.save(c1);  
	    session.save(r2);  session.save(c2);
	 
	    tx.commit();   session.close();    
	    System.out.println("Objects saved");
	}  
}
