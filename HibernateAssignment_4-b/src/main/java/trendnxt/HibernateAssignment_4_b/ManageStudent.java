package trendnxt.HibernateAssignment_4_b;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;


public class ManageStudent {

private static SessionFactory factory;
	
	public static void main(String[] args) {
		
	    factory = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();
	      
	      ManageStudent MS = new ManageStudent();
	      /* Let us have a set of courses for the first student  */
	      Course course1=new Course();
	      course1.setCourseName("Economics");
	      Course course2=new Course();
	      course2.setCourseName("French");
	      Course course3=new Course();
	      course3.setCourseName("Entrepreneurship");
	      Set<Course> setOfCourses1=new HashSet<Course>();
	      
	      setOfCourses1.add(course1);
	      setOfCourses1.add(course2);
	      
	      
	      Set<Course> setOfCourses2=new HashSet<Course>();
	      setOfCourses2.add(course2);
	      setOfCourses2.add(course3);
	      
	     
	      /* Add student records in the database */
	      Integer studentID1 = MS.addStudent("Manoj", "Kumar",setOfCourses1);

	      /* Add another student record in the database */
	      Integer studentID2 = MS.addStudent("Dilip", "Kumar",setOfCourses2);

	      /* List down all the students */
	      MS.listStudents();

	      /* Delete a student from the database */
	      MS.deleteStudent(studentID2);

	      /* List down all the students */
	      MS.listStudents();

	   }
		
	/* Method to add a student record in the database */
	   public Integer addStudent(String fname, String lname,Set cour){
	      
		   Session session = factory.openSession();
	      Transaction tx = null;
	      Integer studentID = null;
	      
	      try {
	         tx = session.beginTransaction();
	         Student student = new Student(fname, lname);
	         student.setCourses(cour);
	         studentID = (Integer) session.save(student); 
	         tx.commit();
	      } catch (HibernateException e) {
	         e.printStackTrace(); 
	      } finally {
	         session.close(); 
	      }
	      return studentID;
	   }

	   /* Method to list all the student's detail */
	   public void listStudents( ){

		   Session session = factory.openSession();
	      Transaction tx = null;
	      
	      try {
	         tx = session.beginTransaction();
	         List students = session.createQuery("FROM Student").list(); 
	         for (Iterator iterator1 = students.iterator(); iterator1.hasNext();){
	            Student student = (Student) iterator1.next(); 
	            System.out.println("First Name: " + student.getStudentFirstName()); 
	            System.out.println("  Last Name: " + student.getStudentLastName()); 
	            Set courses = student.getCourses();
	            for (Iterator iterator2 = courses.iterator(); iterator2.hasNext();){
	            	Course courseName = (Course) iterator2.next(); 
	               System.out.println("Course: " +courseName.getCourseName() ); 
	            }
	         }
	         tx.commit();
	      } catch (HibernateException e) {
	         if (tx!=null) tx.rollback();
	         e.printStackTrace(); 
	      } finally {
	         session.close(); 
	      }
	   }
	   
	   /* Method to delete a student from the records */
	   public void deleteStudent(Integer StudentID){
		   
	      Session session = factory.openSession();
	      Transaction tx = null;
	      
	      try {
	         tx = session.beginTransaction();
	         Student student = (Student)session.get(Student.class, StudentID); 
	         session.delete(student); 
	         tx.commit();
	      } catch (HibernateException e) {
	         e.printStackTrace(); 
	      } finally {
	         session.close(); 
	      }
	   }
}
