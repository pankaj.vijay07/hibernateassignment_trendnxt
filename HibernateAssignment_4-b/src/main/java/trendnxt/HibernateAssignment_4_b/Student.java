package trendnxt.HibernateAssignment_4_b;

import java.util.Set;

public class Student {

	private int studentId;
	private String studentFirstName;
	private String studentLastName;
	private Set courses;
	
	public Student() {
		super();
	}
	
	public Student(String studentFirstName, String studentLastName) {
		super();
		this.studentFirstName = studentFirstName;
		this.studentLastName = studentLastName;
	}

	public int getStudentId() {
		return studentId;
	}
	public void setStudentId(int studentId) {
		this.studentId = studentId;
	}
	
	public String getStudentFirstName() {
		return studentFirstName;
	}
	public void setStudentFirstName(String studentFirstName) {
		this.studentFirstName = studentFirstName;
	}
	public String getStudentLastName() {
		return studentLastName;
	}
	public void setStudentLastName(String studentLastName) {
		this.studentLastName = studentLastName;
	}
	public Set getCourses() {
		return courses;
	}
	public void setCourses(Set courses) {
		this.courses = courses;
	}
	
}
