package trendnxt.assignment.Hibernate1a;

import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.classic.Session;


public class ManageEmployeeClass {

	private static SessionFactory factory;
	
	public static void main(String[] args) {

		factory=new Configuration().configure().buildSessionFactory();
		Session session=factory.openSession();
		Transaction tx=null;
		
		tx=session.beginTransaction();
		
		Employee emp1=new Employee();
		emp1.setName("Pankaj Vijay");
		emp1.setDesignation("Project Engineer");
		emp1.setSalary(20000);
		session.save(emp1);
		
		Employee emp2=new Employee();
		emp2.setName("Ashok Saxena");
		emp2.setDesignation("Project Engineer");
		emp2.setSalary(15000);
		session.save(emp2);
		
		Employee emp3=new Employee();
		emp3.setName("Pooja Mishra");
		emp3.setDesignation("Manager");
		emp3.setSalary(50000);
		session.save(emp3);
		
		Employee emp4=new Employee();
		emp4.setName("Atul Sharma");
		emp4.setDesignation("Delivery Manager");
		emp4.setSalary(90000);
		session.save(emp4);
		
		Employee emp5=new Employee();
		emp5.setName("Aaushi Gupta");
		emp5.setDesignation("Architect");
		emp5.setSalary(55000);
		session.save(emp5);
		
		tx.commit();
		System.out.println("records added successfully to the database");
	}

}
