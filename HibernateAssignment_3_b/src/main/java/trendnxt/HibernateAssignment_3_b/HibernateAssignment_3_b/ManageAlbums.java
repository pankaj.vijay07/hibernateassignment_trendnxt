package trendnxt.HibernateAssignment_3_b.HibernateAssignment_3_b;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class ManageAlbums {

private static SessionFactory factory;
	
	public static void main(String[] args) {

factory=new Configuration().configure().buildSessionFactory();

Session session=factory.openSession();
Transaction tx=session.beginTransaction();

Map<String,List<String>> mp=new HashMap<String,List<String>>();
ArrayList<String> listOfPhotos1=new ArrayList<String>();
listOfPhotos1.add("e1");
listOfPhotos1.add("e2");

ArrayList<String> listOfPhotos2=new ArrayList<String>();
listOfPhotos2.add("m1");
listOfPhotos2.add("m2");

ArrayList<String> listOfPhotos3=new ArrayList<String>();
listOfPhotos3.add("r1");
listOfPhotos3.add("r2");

ArrayList<String> listOfPhotos4=new ArrayList<String>();
listOfPhotos4.add("dp1");
listOfPhotos4.add("dp2");

ArrayList<String> listOfPhotos5=new ArrayList<String>();
listOfPhotos5.add("ny1");
listOfPhotos5.add("ny2");

ArrayList<String> listOfPhotos6=new ArrayList<String>();
listOfPhotos6.add("h1");
listOfPhotos6.add("h2");

ArrayList<String> listOfPhotos7=new ArrayList<String>();
listOfPhotos7.add("rkh1");
listOfPhotos7.add("rkh2");

ArrayList<String> listOfPhotos8=new ArrayList<String>();
listOfPhotos8.add("b1");
listOfPhotos8.add("b2");

ArrayList<String> listOfPhotos9=new ArrayList<String>();
listOfPhotos9.add("bj1");
listOfPhotos9.add("bj2");

ArrayList<String> listOfPhotos10=new ArrayList<String>();
listOfPhotos10.add("hi1");
listOfPhotos10.add("hi2");

mp.put("Engagement", listOfPhotos1);
mp.put("Marriage", listOfPhotos2);
mp.put("Reception", listOfPhotos3);

Map<String,List<String>> mp2=new HashMap<String,List<String>>();
mp2.put("Durga Puja", listOfPhotos4);
mp2.put("New Year", listOfPhotos5);
mp2.put("Holi", listOfPhotos6);
mp2.put("Rakhi", listOfPhotos7);

Map<String,List<String>> mp3=new HashMap<String,List<String>>();
mp3.put("Birthday Celebration", listOfPhotos8);

Map<String,List<String>> mp4=new HashMap<String,List<String>>();
mp4.put("Shyam Bhajan Sandhya", listOfPhotos9);

Map<String,List<String>> mp5=new HashMap<String,List<String>>();
mp5.put("House Inaugration", listOfPhotos10);
		
Album alb1=new Album();
alb1.setAname("Marriage Album");
alb1.setPhotos(mp);

Album alb2=new Album();
alb2.setAname("Indian Festivals Album");
alb2.setPhotos(mp);


Album alb3=new Album();
alb3.setAname("Birthday Celebration Album");
alb3.setPhotos(mp);


Album alb4=new Album();
alb4.setAname("Shyam Bhajan Sandhya Album");
alb4.setPhotos(mp);


Album alb5=new Album();
alb5.setAname("Inaugration  Album");
alb5.setPhotos(mp);

tx.commit();
session.close();


	}

}
